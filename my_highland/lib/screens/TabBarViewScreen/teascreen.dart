import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:my_highland/widget/menu_teas.dart';

import '../../controllers/product_controller.dart';

class TeaScreen extends StatefulWidget {
  const TeaScreen({Key? key}) : super(key: key);

  @override
  State<TeaScreen> createState() => _TeaScreenState();
}

class _TeaScreenState extends State<TeaScreen> {
  final ProductControllerTea productController = Get.put(ProductControllerTea());

  @override
  Widget build(BuildContext context) {
    return  Column(
      children: [
        Expanded(
            child: Obx(() => ListView(
              children: [
                GridView.builder(
                  physics: ScrollPhysics(),
                  itemCount: productController.productList.length,
                  itemBuilder: (BuildContext context, int index) {
                    return MenuTeas(productController.productList[index]);
                  },
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    mainAxisSpacing: 10,
                    crossAxisSpacing: 5,
                  ),
                  padding: EdgeInsets.all(10),
                  shrinkWrap: true,
                ),
              ],
            ))),
      ],);
  }
}
