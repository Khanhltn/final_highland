import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:my_highland/widget/menu_products.dart';

import '../../controllers/product_controller.dart';

class CoffeeScreen extends StatefulWidget {
  const CoffeeScreen({Key? key}) : super(key: key);

  @override
  State<CoffeeScreen> createState() => _CoffeeScreenState();
}

class _CoffeeScreenState extends State<CoffeeScreen> {
  final ProductControllerCoffee productController = Get.put(ProductControllerCoffee());

  @override
  Widget build(BuildContext context) {
    return  Column(
      children: [
        Expanded(
            child: Obx(() => ListView(
              children: [
                GridView.builder(
                  physics: ScrollPhysics(),
                  itemCount: productController.productList.length,
                  itemBuilder: (BuildContext context, int index) {
                    return MenuProducts(productController.productList[index]);
                  },
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    mainAxisSpacing: 10,
                    crossAxisSpacing: 5,
                  ),
                  padding: EdgeInsets.all(10),
                  shrinkWrap: true,
                ),
              ],
            ))),
      ],);
  }
}
