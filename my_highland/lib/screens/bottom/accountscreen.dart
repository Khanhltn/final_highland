import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_highland/models/user_model.dart';
import 'package:my_highland/screens/accounts/informationHighland.dart';
import '../accounts/Loginscreen.dart';



class AccountScreen extends StatefulWidget {
  const AccountScreen({Key? key}) : super(key: key);

  @override
  State<AccountScreen> createState() => _AccountScreenState();
}

class _AccountScreenState extends State<AccountScreen> {
  User? user = FirebaseAuth.instance.currentUser;
  UserModel loggedInUser =  UserModel();

  @override
  void initState() {
    super.initState();
    FirebaseFirestore.instance.collection("users").doc(user!.uid).get().then((value) {
      this.loggedInUser = UserModel.fromMap(value.data());
      setState(() {

      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        color: Color(0xFFCFD8DC),
        child: ListView(
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width,
                    color: Colors.white,
                    child: Row(
                      children: [
                        SizedBox(
                            height: MediaQuery.of(context).size.height/6,
                            child: Padding(
                              padding: EdgeInsets.only(right: 20),
                              child: CircleAvatar(
                                backgroundColor: Colors.white,
                                child: Image.network('https://cn.i.cdn.ti-platform.com/cnapac/content/701/showpage/we-bare-bears/sa/showicon.png'),
                              ),
                            )),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: [
                              Text(
                                  "\Tên: ${loggedInUser.firstname} ${loggedInUser.secondName}",
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.w500
                                  )),
                              Text(
                                "\Email: ${loggedInUser.email}",
                                style: TextStyle(fontSize: 15),
                              ),
                            ],
                          ),),

                        ActionChip(
                            label: Text('Log out'),
                            onPressed: (){
                          logout(context);
                        }),
                      ],
                    ),),

                  Container(
                    height: MediaQuery.of(context).size.height/5,
                    color: Colors.white,
                    margin: EdgeInsets.only(top: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(height: MediaQuery.of(context).size.height/20,),
                        Row(
                          children: [
                            Padding(padding: EdgeInsets.only(left: 5), child: Icon(Icons.circle_notifications),),
                            SizedBox(width: 5,),
                            Expanded(child: Text('Thông tin chung',
                              style: TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: 25,
                              ),),),
                          ],
                        ),
                        SizedBox(height: MediaQuery.of(context).size.height/50,),
                        Expanded(child: GestureDetector(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Padding(
                                padding: EdgeInsets.only(left: 5),
                                child: Text('Về Highlands Coffee',
                                  style: TextStyle(
                                    fontWeight: FontWeight.w300,
                                    fontSize: 18,
                                  ),
                                ),),
                              Icon(Icons.arrow_forward_ios,color: Colors.grey, size: 18,)
                            ],
                          ),
                          onTap: (){
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const InformatioHighland()),
                            );
                          },
                        ),),
                        Expanded(child: GestureDetector(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Padding(
                                padding: EdgeInsets.only(left: 5),
                                child: Text('Danh sách quán',
                                  style: TextStyle(
                                    fontWeight: FontWeight.w300,
                                    fontSize: 18,
                                  ),),),
                              Icon(Icons.arrow_forward_ios,color: Colors.grey, size: 18,)
                            ],
                          ),
                          onTap: (){
                          },
                        ),),
                      ],
                    ),
                  ),

                  Container(
                    height: MediaQuery.of(context).size.height/5,
                    color: Colors.white,
                    margin: EdgeInsets.only(top: 10,),
                    child: Column(
                      children: [
                        SizedBox(height: MediaQuery.of(context).size.height/20,),
                        Row(
                          children: [
                            Padding(
                              padding: EdgeInsets.only(left: 5),
                              child: Icon(Icons.build_circle_outlined
                              ),),
                            SizedBox(width: 5,),
                            Expanded(child: Text('Trung tâm hổ trợ',
                              style: TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: 25,
                              ),),),
                          ],
                        ),
                        SizedBox(height: MediaQuery.of(context).size.height/50),
                        Expanded(child: GestureDetector(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Padding(
                                padding: EdgeInsets.only(left: 5),
                                child: Text('Câu hỏi thường gặp',
                                  style: TextStyle(
                                    fontWeight: FontWeight.w300,
                                    fontSize: 18,
                                  ),
                                ),),
                              Icon(Icons.arrow_forward_ios,color: Colors.grey, size: 18,)
                            ],
                          ),
                          onTap: (){
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const InformatioHighland()),
                            );
                          },
                        ),),
                        Expanded(child: GestureDetector(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Padding(
                                padding: EdgeInsets.only(left: 5),
                                child: Text('Phản hồi và hổ trợ',
                                  style: TextStyle(
                                    fontWeight: FontWeight.w300,
                                    fontSize: 18,
                                  ),),),
                              Icon(Icons.arrow_forward_ios,color: Colors.grey, size: 18,)
                            ],
                          ),
                          onTap: (){
                          },
                        ),),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 10,),
                    height: MediaQuery.of(context).size.height/4,
                    color: Colors.white,
                    child: Column(
                      children: [
                        SizedBox(height: MediaQuery.of(context).size.height/20,),
                        Row(
                          children: [
                            Padding(
                              padding: EdgeInsets.only(left: 5),
                              child: Icon(Icons.more_horiz,),
                            ),
                            SizedBox(width: 5,),
                            Expanded(child: Text('Khác',
                              style: TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: 25,
                              ),),),
                          ],
                        ),
                        SizedBox(height: MediaQuery.of(context).size.height/50,),
                        Expanded(child: GestureDetector(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Padding(
                                padding: EdgeInsets.only(left: 5),
                                child: Text('Ngôn ngữ',
                                  style: TextStyle(
                                    fontWeight: FontWeight.w300,
                                    fontSize: 18,
                                  ),
                                ),
                              ),
                              Icon(Icons.arrow_forward_ios,color: Colors.grey, size: 18,)
                            ],
                          ),
                          onTap: (){
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const InformatioHighland()),
                            );
                          },
                        ),),
                        Expanded(child: GestureDetector(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Padding(
                                padding: EdgeInsets.only(left: 5),
                                child: Text('Điều khoản & điều khoản',
                                  style: TextStyle(
                                    fontWeight: FontWeight.w300,
                                    fontSize: 18,
                                  ),),
                              ),
                              Icon(Icons.arrow_forward_ios,color: Colors.grey, size: 18,)
                            ],
                          ),
                          onTap: (){
                          },
                        ),),
                        Expanded(child: GestureDetector(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Padding(
                                padding: EdgeInsets.only(left: 5),
                                child: Text('Về ứng dụng',
                                  style: TextStyle(
                                    fontWeight: FontWeight.w300,
                                    fontSize: 18,
                                  ),),),
                              Icon(Icons.arrow_forward_ios,color: Colors.grey, size: 18,)
                            ],
                          ),
                          onTap: (){
                          },
                        ),),
                        SizedBox(height: MediaQuery.of(context).size.height/50,),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(15),
                    child: GestureDetector(
                    child: Row(
                      children: [
                        SizedBox(width: 5,),
                        Icon(Icons.exit_to_app, size: 19,),
                        SizedBox(width: 5,),
                        Text('Thoát Ứng Dụng',
                          style: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 18,
                          ),),
                      ],
                    ),
                    onTap: (){
                      Navigator.pop(context);
                    },
                  ),),
                ],
              ),);
  }
  Future<void> logout(BuildContext context) async {
    await FirebaseAuth.instance.signOut();
    Navigator.of(context).pushReplacement(
      MaterialPageRoute(builder: (context) => LoginScreen())
    );
  }
}
